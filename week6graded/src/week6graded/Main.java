package week6graded;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Movie> allMovies=new ArrayList<>();
		allMovies.add(new Movie(1, "AVATAR 2", "upcoming", "India", 0));
		allMovies.add(new Movie(2, "Rowdy boys", "movies in theatre", "india", 6));
		allMovies.add(new Movie(3, "spiderman no way home", "movies in theatre", "US", 9));
		allMovies.add(new Movie(4, "Hero", "moviesintheatre", "india", 7));
		allMovies.add(new Movie(5, "KGF2", "upcoming", "India", 0));		
		allMovies.add(new Movie(6, "Adipurush", "upcoming", "india", 0));
		
		System.out.println(allMovies);
		//singleton
		MoviesService service=MoviesService.getServiceObject();
		service.setAllMovies(allMovies);
		
		System.out.println(service.getComingSoonMovies());
		
		System.out.println(service.getMoviesInTheatres());
		
		System.out.println(service.getTopRatedIndian());
		
		System.out.println(service.getTopRatedMovies());
		
		System.out.println(service.getMovie(1));
		Movie movie=service.getMovie(2);
		service.addFavMovie(movie);
		System.out.println(service.getFavMovies());
		service.removeFavMovie(movie);
		System.out.println(service.getFavMovies());
	}
}
